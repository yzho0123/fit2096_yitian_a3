// Fill out your copyright notice in the Description page of Project Settings.


#include "PressurePad.h"
#include "Components/WidgetComponent.h"
#include "PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"

// Sets default values
APressurePad::APressurePad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Every Actor contains a root component. We initialize this as a scene component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	// We create a mesh that will be our visual representation of the object and attachit to the root
	PressurePadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	PressurePadMesh->SetupAttachment(RootComponent);
	// We create a hitbox that will be our collider that we can walk over to activate.This is also attached to root
	PressurePadHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	PressurePadHitBox->SetupAttachment(RootComponent);
	
	ReduceHealthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("ReduceHealthText"));
	ReduceHealthWidget->SetupAttachment(RootComponent);
}

void APressurePad::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
 AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
 bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor && OtherActor != this && Cast<APlayerCharacter>(OtherActor))
	{
		Cast<APlayerCharacter>(OtherActor)->ChangeHealth(HealthAlteration);
		if(HealthAlteration>0)
		{
			FVector SpawnLocation = OverlappedComponent->GetComponentLocation();
			if(NS_Treatment)
			{
				UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NS_Treatment, SpawnLocation);
			}
			if(SB_Treatment)
            {UGameplayStatics::PlaySoundAtLocation(GetWorld(), SB_Treatment,SpawnLocation); }

		}
	}
}

void APressurePad::OnHitboxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
 AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	if(OtherActor && OtherActor != this && Cast<APlayerCharacter>(OtherActor))
	{
		Cast<APlayerCharacter>(OtherActor)->ResetDamage();
	}
}



// Called when the game starts or when spawned
void APressurePad::BeginPlay()
{
	Super::BeginPlay();
	PressurePadHitBox->OnComponentBeginOverlap.AddDynamic(this, &APressurePad::OnHitboxOverlapBegin);
    PressurePadHitBox->OnComponentEndOverlap.AddDynamic(this, &APressurePad::OnHitboxOverlapEnd);

}

// Called every frame
void APressurePad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

