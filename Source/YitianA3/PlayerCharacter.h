// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "CharacterPlayerController.h"
#include "combatComponent.h"
#include "CharacterHUD.h"
#include "Weapon.h"
#include "NiagaraSystem.h"
#include "PlayerCharacter.generated.h"


UCLASS()
class YITIANA3_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	APlayerCharacter();

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(EditAnywhere, Category = Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;

	
	UPROPERTY(EditAnywhere)
	float Health = 100.0f;

	UPROPERTY(EditAnywhere)
	float HealthAlteration = -1.0f;

	UPROPERTY(EditAnywhere)
	bool TakeDamage = false;

	UFUNCTION()
	void ChangeHealth(float amount);

	UFUNCTION()
	void ResetDamage();

	UFUNCTION()
	void PlayerDeath();

	UPROPERTY(EditAnywhere, Category = "Bang")
	UNiagaraSystem* NS_Bang;

	UPROPERTY(EditAnywhere, Category = "Bang")
	USoundBase* SB_Bang;

	UPROPERTY(EditAnywhere, Category = "Push")
	USoundBase* SB_Push;


	
protected:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	/** Weapon */
	UPROPERTY(VisibleAnywhere, Category = "Weapon Properties")
	UStaticMeshComponent* WeaponMesh;
	
	float MovementSpeed;
	float TurnRateGamepad;
	/*** Player health*/

	
	class ACharacterPlayerController* CharacterPlayerController;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	/** make pull and push */
	void OnPullAction();
	/** make Fire */
	virtual void FireCannon();
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	/** Equip */
	void EquipButtonPressed();
	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	TouchData	TouchItem;
	
	UPROPERTY(VisibleAnywhere)
	class UcombatComponent* Combat;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	
	
	bool Trace(UWorld* World,TArray<AActor*>& ActorsToIgnore,const FVector& Start,const FVector& End,FHitResult& HitOut,ECollisionChannel CollisionChannel,bool ReturnPhysMat);

	void CallMyTrace();
    void ProcessTraceHit(FHitResult& HitOut);
	virtual void PostInitializeComponents() override;
	UPROPERTY(EditAnywhere)
	class AWeapon* OverlappingWeapon;
public:	
	void SetOverlappingWeapon(AWeapon* Weapon);
	/** Returns Mesh1P subobject **/

};
