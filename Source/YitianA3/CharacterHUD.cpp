// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHUD.h"
void ACharacterHUD::BeginPlay()
{
	Super::BeginPlay();

	AddCharacterOverlay();
	
}

void ACharacterHUD::AddCharacterOverlay()
{
	APlayerController* PlayerController = GetOwningPlayerController();
	if (PlayerController && CharacterOverlayClass)
	{
		CharacterOverlay = CreateWidget<UCharacterOverlay>(PlayerController, CharacterOverlayClass);
		UE_LOG(LogTemp, Warning, TEXT("To Player "));
		CharacterOverlay->AddToViewport();
	}
}
