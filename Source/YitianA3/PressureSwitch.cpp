// Fill out your copyright notice in the Description page of Project Settings.


#include "PressureSwitch.h"

#include "PlayerCharacter.h"


// Sets default values
APressureSwitch::APressureSwitch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// We create a mesh that will be our visual representation of the object and attach it to the root
	PressurePadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	PressurePadMesh->SetupAttachment(RootComponent);

	// We create a hitbox that will be our collider that we can walk over to activate. This is also attached to root
	PressurePadHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	PressurePadHitBox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APressureSwitch::BeginPlay()
{
	Super::BeginPlay();
	PressurePadHitBox->OnComponentBeginOverlap.AddDynamic(this, &APressureSwitch::OnHitboxOverlapBegin);
}

// Called every frame
void APressureSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APressureSwitch::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* PlayerCharacter=Cast<APlayerCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(Cast<APlayerCharacter>(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlap Has Begun"));
		PressurePadHitBoxDelegate.Broadcast();
	}
}