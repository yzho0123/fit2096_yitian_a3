// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "NiagaraSystem.h"
#include "Projectile.generated.h"

UCLASS()
class YITIANA3_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BulletMesh;
	UPROPERTY(EditAnywhere)
	AActor* Parent;
	UPROPERTY(EditAnywhere)
	float MovementSpeed;
	UPROPERTY(EditAnywhere)
	float MaximumLifetime;
	UPROPERTY(EditAnywhere)
	float CurrentLifetime;
	UPROPERTY(EditAnywhere)
	int playercount;

	UPROPERTY(EditAnywhere, Category = "Explosion")
	UNiagaraSystem* NS_Bang;

	UPROPERTY(EditAnywhere, Category = "Explosion")
	USoundBase* SB_Bang;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY
	(EditAnywhere)
	float HealthAlteration = 0.0f;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void playerdeath();
	
	
	UPROPERTY(EditAnywhere)
	bool TakeDamage = false;
	UPROPERTY(EditAnywhere)
	bool Destroy1 = false;
	UFUNCTION()
	void OnOverLap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
