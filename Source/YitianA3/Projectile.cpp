// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "NiagaraFunctionLibrary.h"
#include"Kismet/GameplayStatics.h"
#include "PlayerCharacter.h"

// Sets default values
AProjectile::AProjectile()
{
 	
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	BulletMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	RootComponent = BulletMesh;
	Parent = nullptr;
	MovementSpeed = 3000;
	MaximumLifetime = 5;
	CurrentLifetime = 0;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	//highlight:cast me a lot of time to find this lin issue!!!
	BulletMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnOverLap);
	playercount = 0;
	Destroy1=false;
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector CurrentLocation = GetActorLocation();
	FVector Forward = GetActorForwardVector();
	FVector Movement = (Forward * MovementSpeed * DeltaTime);
	FVector NewLocation = CurrentLocation + Movement;
	SetActorLocation(NewLocation);
	
	CurrentLifetime += DeltaTime;
	if(CurrentLifetime >= MaximumLifetime)
	{
		UE_LOG(LogTemp, Warning, TEXT(" Projectile time out and  destroyed") );
		Destroy();
	}
	
}

void AProjectile::OnOverLap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
	{if(OtherActor && OtherActor != this)
	{if(!Parent)
		return;
		if(Parent == OtherActor)
			return;
		FVector SpawnLocation = OverlappedComponent->GetComponentLocation();
		if (NS_Bang)
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NS_Bang, SpawnLocation);
		}
		if(SB_Bang)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SB_Bang,SpawnLocation);
		}
		Destroy();
		
	}
	
	APlayerCharacter* PlayerCharacter=Cast<APlayerCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if(Cast<APlayerCharacter>(OtherActor))
	{
		

		OtherActor->Destroy();
		
		
		//Cast<APlayerCharacter>(OtherActor)->ChangeHealth(HealthAlteration);
	}
}

