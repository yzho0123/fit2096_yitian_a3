// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GameFramework/PlayerController.h"
#include "CharacterOverlay.h"
#include "CharacterHUD.generated.h"

/**
 * 
 */
UCLASS()
class YITIANA3_API ACharacterHUD : public AHUD
{
	GENERATED_BODY()
public:
	

	UPROPERTY(EditAnywhere, Category = "Player Stats")
	TSubclassOf<class UUserWidget> CharacterOverlayClass;

	class UCharacterOverlay* CharacterOverlay;
protected:
	virtual void BeginPlay() override;
	void AddCharacterOverlay();
};
