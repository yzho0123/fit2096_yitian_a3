// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPlayerController.h"

#include "GameFramework/Character.h"


void ACharacterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	CharacterHUD = Cast<ACharacterHUD>(GetHUD());
}

void ACharacterPlayerController::SetHUDHealth(float Health, float MaxHealth)
{
	CharacterHUD=CharacterHUD== nullptr ? Cast<ACharacterHUD>(GetHUD()):CharacterHUD;
	bool bHUDValid = CharacterHUD
	&& CharacterHUD->CharacterOverlay
	&& CharacterHUD->CharacterOverlay->HealthBar
	&& CharacterHUD->CharacterOverlay->HealthText;
	if(bHUDValid)
	{
		const float HealthParcent = Health/MaxHealth;
		CharacterHUD->CharacterOverlay->HealthBar->SetPercent(HealthParcent);
		FString HealthText = FString::Printf(TEXT("%d/%d"),FMath::CeilToInt(Health), FMath::CeilToInt(MaxHealth));
		CharacterHUD->CharacterOverlay->HealthText->SetText(FText::FromString(HealthText));
	}
	
}