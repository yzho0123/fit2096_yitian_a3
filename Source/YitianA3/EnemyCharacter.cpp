// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 1, 0);
	
	// Every Actor contains a root component. We initialize this as a scene component

	
	// We create a hitbox that will be our collider that we can walk over to activate.This is also attached to root
	InflectionHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	InflectionHitBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	InflectionHitBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	InflectionHitBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	InflectionHitBox->SetupAttachment(RootComponent);

}

void AEnemyCharacter::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor && OtherActor != this && Cast<APlayerCharacter>(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("Set Actor health"));
		Cast<APlayerCharacter>(OtherActor)->ChangeHealth(HealthAlteration);
	}
}

void AEnemyCharacter::OnHitboxOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	if(OtherActor && OtherActor != this && Cast<APlayerCharacter>(OtherActor))
	{
		Cast<APlayerCharacter>(OtherActor)->ResetDamage();
	}
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	InflectionHitBox->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::OnHitboxOverlapBegin);
	InflectionHitBox->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::OnHitboxOverlapEnd);
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

