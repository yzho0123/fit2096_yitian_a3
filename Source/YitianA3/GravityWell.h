// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interact.h"
#include "GameFramework/Actor.h"
#include "GravityWell.generated.h"

UENUM(BlueprintType)
enum class EGravityType:uint8
{
	GE_Pull UMETA(DisplayName=”Pull”),
	GE_Push UMETA(DisplayName=”Push”),
	GE_On UMETA(DisplayName=”On”),
	GE_Off UMETA(DisplayName=”Off”),
   };

UCLASS()
class YITIANA3_API AGravityWell : public AActor, public IInteract
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGravityWell();
	
	UPROPERTY
	(EditAnywhere, BlueprintReadWrite, Category = "interact")
	EGravityType GravityType;

	UPROPERTY
	(EditAnywhere, BlueprintReadWrite, Category = "interact")
	float sweepSize = 1000.0f
	;
	UPROPERTY
	(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	UStaticMeshComponent* BaseMesh;
	

	virtual void Interact_Implementation() override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
