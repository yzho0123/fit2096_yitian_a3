// Fill out your copyright notice in the Description page of Project Settings.


#include "GravityWell.h"

// Sets default values
AGravityWell::AGravityWell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	RootComponent = BaseMesh;
}

void AGravityWell::Interact_Implementation()
{
	IInteract::Interact_Implementation();
	TArray<FHitResult> OutHits;
	FVector location = GetActorLocation();
	FCollisionShape ExplosionSphere = FCollisionShape::MakeSphere(sweepSize);
	//DrawDebugSphere(GetWorld(), location, ExplosionSphere.GetSphereRadius(), 50, FColor::Red, false, 1.0f, 0,0);

	if(GetWorld()->SweepMultiByChannel(OutHits, location,location, FQuat::Identity, ECC_WorldStatic, ExplosionSphere)){
		for(auto& Hit : OutHits){
			UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(Hit.GetActor()->GetRootComponent());
			if(Mesh){
				switch(GravityType){
				case EGravityType::GE_Pull:
					Mesh->AddRadialImpulse(location, sweepSize, -2000.0f, ERadialImpulseFalloff::RIF_Linear, true);
					break;
				case EGravityType::GE_Push:
					Mesh->AddRadialImpulse(location, sweepSize, 2000.0f, ERadialImpulseFalloff::RIF_Linear, true);
					break;
				case EGravityType::GE_On:
					Mesh->SetEnableGravity(true);
					break;
				case EGravityType::GE_Off:
					Mesh->SetEnableGravity(false);
					break;
				default:
					UE_LOG(LogClass, Warning, TEXT("No Gravity Type"));
				}
			}
		}
	}
}


// Called when the game starts or when spawned
void AGravityWell::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGravityWell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

