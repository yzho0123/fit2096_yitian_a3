// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CharacterHUD.h"
#include "combatComponent.h"
#include "CharacterOverlay.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "CharacterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class YITIANA3_API ACharacterPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	void SetHUDHealth(float Health, float MaxHealth);
protected:
	virtual void BeginPlay() override;

private:
	class ACharacterHUD* CharacterHUD;
};
