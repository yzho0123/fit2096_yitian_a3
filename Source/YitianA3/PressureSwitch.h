// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "PressureSwitch.generated.h"

UCLASS()
class YITIANA3_API APressureSwitch : public AActor
{
	GENERATED_BODY()
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPressurePadActivated);


public:	
	// Sets default values for this actor's properties
	APressureSwitch();
	
	UPROPERTY (BlueprintAssignable)
	FOnPressurePadActivated PressurePadHitBoxDelegate;
	//create value to static mesh component and Box Component
	UPROPERTY (EditAnywhere)
	UStaticMeshComponent* PressurePadMesh;
	//The Box Component is declared in a similar fashion
	UPROPERTY (EditAnywhere)
	UBoxComponent* PressurePadHitBox;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent,AActor* OtherActor,UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,bool bFromSweep,const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
