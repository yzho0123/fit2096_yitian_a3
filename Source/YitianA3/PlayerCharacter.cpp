// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Projectile.h"
#include "Interact.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"



// Sets default values
APlayerCharacter::APlayerCharacter()
{
	
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;
	// set our turn rate for input
	TurnRateGamepad=50.0f;
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	//camera
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetMesh());	CameraBoom->TargetArmLength = 600.f;
	CameraBoom->bUsePawnControlRotation = false;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// weapon
	
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetupAttachment(RootComponent);
	Combat = CreateDefaultSubobject<UcombatComponent>(TEXT("CombatComponent"));
	Combat->SetIsReplicated(true);

	//pro
	static ConstructorHelpers::FObjectFinder<UBlueprint> ProjectileBlueprint(TEXT("Blueprint'/Game/Blueprints/BP_Projectile.BP_Projectile'"));
	if (ProjectileBlueprint.Object){
		ProjectileClass = ProjectileBlueprint.Object->GeneratedClass;
	}
}



void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(TakeDamage) {
		Health += HealthAlteration;
		if(Health > 100)
		{
			Health = 100.0f;
		}
			
		if(Health <=0)
		{
			Health = 0.0f;
			PlayerDeath();
		}
			
	}
}



void APlayerCharacter::ChangeHealth(float amount)
{
	TakeDamage = true;
	HealthAlteration = amount; 
}

void APlayerCharacter::ResetDamage()
{
	TakeDamage = false;
	HealthAlteration = 0.0f; 
}

void APlayerCharacter::PlayerDeath()
{
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->JumpZVelocity = 0.f;
	GetCharacterMovement()->AirControl = 0.f;
	GetCharacterMovement()->MaxWalkSpeed = 0.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 0.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 0.f;
	Destroy();

}
// Called when the game starts or when spawned


void APlayerCharacter::OnPullAction()
{
	CallMyTrace();
}

void APlayerCharacter::FireCannon()
{
	FVector CannonOffset = FVector(50, 0, 60);
	CannonOffset = WeaponMesh->GetComponentRotation().RotateVector(CannonOffset);
	FVector PlayerPosition = GetActorLocation();
	FVector StartPosition = PlayerPosition;
	FRotator PlayerRotation = GetActorRotation();
	AProjectile* SpawnedBullet = (AProjectile*) GetWorld()->SpawnActor(ProjectileClass, &StartPosition,&PlayerRotation);
	SpawnedBullet->Parent = this;
	SpawnedBullet->SetActorRotation(GetActorRotation());
	FVector SpawnLocation = WeaponMesh->GetComponentLocation()+(10,0,0);
	if (NS_Bang)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NS_Bang, SpawnLocation);
	}
	if(SB_Bang)
    {
     UGameplayStatics::PlaySoundAtLocation(GetWorld(), SB_Bang,SpawnLocation);
    }

}

void APlayerCharacter::MoveForward(float Value)
{
	
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, 1, 0);
		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
	
}

void APlayerCharacter::MoveRight(float Value)
{
	
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, 1, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}

}

void APlayerCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}
// Not achieved weapon system.... 
void APlayerCharacter::EquipButtonPressed()
{
	
	if (Combat && HasAuthority())
	{
		Combat->EquipWeapon(OverlappingWeapon);
		UE_LOG(LogTemp, Warning, TEXT("E"));
		
	}

}

void APlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if(Combat)
	{
		Combat->Character = this;
	}
}


// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("Move Forward / Backward",this,&APlayerCharacter::MoveForward);
	InputComponent->BindAxis("Move Right / Left",this,&APlayerCharacter::MoveRight);
	// Bind fire event
	PlayerInputComponent->BindAction("Pull And Push Action", IE_Pressed, this, &APlayerCharacter::OnPullAction);
	PlayerInputComponent->BindAction("Equip", IE_Pressed, this, &APlayerCharacter::EquipButtonPressed);
	InputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &APlayerCharacter::FireCannon);
}

bool APlayerCharacter::Trace(UWorld* World, TArray<AActor*>& ActorsToIgnore, const FVector& Start, const FVector& End,
	FHitResult& HitOut, ECollisionChannel CollisionChannel, bool ReturnPhysMat)
{
	if (!World)
	{
		return false;
	}
	// Set up our TraceParams object
	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);
	// Should we simple or complex collision?
	TraceParams.bTraceComplex = true;
	// We don't need Physics materials
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;
	// Add our ActorsToIgnore
	TraceParams.AddIgnoredActors(ActorsToIgnore);
	// When we're debugging it is really useful to see where our trace is in the world
	// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
	// (remove these lines to remove the debug - or better create a debug switch!)
	const FName TraceTag("MyTraceTag");
	World->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;
	// Force clear the HitData which contains our results
	HitOut = FHitResult(ForceInit);
	// Perform our trace
	World->LineTraceSingleByChannel (HitOut, //result
	Start, //start
	End, //end
	CollisionChannel, //collision channel
	TraceParams);
	// If we hit an actor, return true
	return (HitOut.GetActor() != NULL);
}

void APlayerCharacter::CallMyTrace()
{// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = GetActorLocation();
	FRotator ActorRotation = GetActorRotation();
	const FVector ActorForwardDirection = FRotationMatrix(ActorRotation).GetUnitAxis(EAxis::X);
	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start+ActorForwardDirection * 1256;
	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);
	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;
	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);
	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if
	(Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false)) {
		// Process our HitData
		if (HitData.GetActor()) {
			UE_LOG(LogClass, Warning, TEXT("We Hit this Actor:. %s"), *HitData.GetActor()->GetName());
			ProcessTraceHit(HitData);
			FVector SpawnLocation = WeaponMesh->GetComponentLocation()+(5,0,0);
			if(SB_Push)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), SB_Push,SpawnLocation);
			}
		} else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log
		}
	} else
	{
		// We did not hit an Actor
	}

}

void APlayerCharacter::ProcessTraceHit(FHitResult& HitOut)
{
	if (HitOut.GetActor()->GetClass()->ImplementsInterface
   (UInteract::StaticClass()))
	{
		IInteract::Execute_Interact(HitOut.GetActor());
	} else
	{
		UE_LOG(LogClass, Warning, TEXT
	   ("Actor is NOT Interactable!"));
	}
}

void APlayerCharacter::SetOverlappingWeapon(AWeapon* Weapon)
{
	
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(false);
	}
	OverlappingWeapon = Weapon;
	if (IsLocallyControlled())
	{
		if (OverlappingWeapon)
		{
			OverlappingWeapon->ShowPickupWidget(true);
		}
	}
}